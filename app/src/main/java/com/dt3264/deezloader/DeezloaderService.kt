package com.dt3264.deezloader

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.Context
import android.content.Intent
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.IBinder
import android.util.ArrayMap
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import io.socket.client.IO
import org.json.JSONObject
import java.util.*

class DeezloaderService : Service() {
    internal lateinit var context: Context
    private var serverThread: Thread? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            Objects.requireNonNull(intent.action)
            getString(R.string.serviceName)
        }
        return START_NOT_STICKY
    }

    // In case the service is deleted or crashes somehow
    override fun onDestroy() {
        stopService()
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        // Used only in case of bound services.
        return null
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        createNotificationChannel()
        prepareNodeServerListeners()
        startService()
    }

    // Notification methods
    private fun startService() {
        if (isServiceRunning) return

        val serverNotifIntent = Intent(applicationContext, BrowserActivity::class.java).apply {
            action = Intent.ACTION_MAIN
            addCategory(Intent.CATEGORY_LAUNCHER)
            addFlags(Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        }

        val serverPendingIntent = PendingIntent.getActivity(this, 0, serverNotifIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val serverNotifBuilder = NotificationCompat.Builder(this, serviceChannelID).apply {
            setContentTitle(resources.getString(R.string.app_name))
            setTicker(resources.getString(R.string.app_name))
            setContentText(resources.getString(R.string.notificationService))
            setSmallIcon(R.mipmap.ic_notification)
            setOngoing(false)
            setContentIntent(serverPendingIntent)
            setCategory(NotificationCompat.CATEGORY_SERVICE)
        }

        startServer()
        startForeground(serviceNotificationID, serverNotifBuilder.build())
    }

    private fun stopService() {
        if (serverThread != null)
            serverThread!!.interrupt()
        serverThread = null
        stopForeground(true)
        stopSelf()
        isServiceRunning = false
    }

    private fun startServer() {
        try {
            serverThread = Thread {
                //The path where we expect the node project to be at runtime.
                val nodeDir = applicationContext.filesDir.absolutePath + "/deezloader"
                NodeNative.startNodeServer(arrayOf("node", "$nodeDir/app.js"))
            }
            serverThread!!.start()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val thisNotificationManager = getSystemService(NotificationManager::class.java)!!

            val servicename = "Service Notification"
            val servicedescription = "The notification present when the Deezloader service is running."
            val serviceimportance = NotificationManager.IMPORTANCE_MIN
            val servicechannel = NotificationChannel(serviceChannelID, servicename, serviceimportance)
            servicechannel.description = servicedescription
            servicechannel.setSound(null, null)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            thisNotificationManager.createNotificationChannel(servicechannel)

            val downloadname = "Download Notifications"
            val downloaddescription = "Notifications providing the status of music downloads."
            val downloadimportance = NotificationManager.IMPORTANCE_DEFAULT
            val downloadchannel = NotificationChannel(downloadChannelID, downloadname, downloadimportance)
            downloadchannel.description = downloaddescription
            downloadchannel.setSound(null, null)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            thisNotificationManager.createNotificationChannel(downloadchannel)
        }
    }

    private fun prepareNodeServerListeners() {
        NodeNative.serviceSocket = IO.socket(serverURL)

        NodeNative.serviceSocket.apply {
            on("siteReady") {
                isServiceRunning = true
            }

            on("openLink") { args ->
                val url = args[0] as String
                val i = Intent(Intent.ACTION_VIEW)
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                i.data = Uri.parse(url)
                startActivity(i)
            }

            on("addToQueue") { args ->
                val updateData = args[0] as JSONObject

                val name = updateData.get("name") as String
                val artist = updateData.get("artist") as String
                val size = updateData.get("size").toString()
                val queueId = updateData.get("queueId").toString()
                val type = updateData.get("type") as String

                newDownloadNotification(type, queueId, name, artist, size)
            }

            // This is used for overall progress on batched downloads
            on("updateQueue") { args ->
                val updateData = args[0] as JSONObject

                val size = updateData.get("size").toString()
                val downloaded = updateData.get("downloaded").toString()
                val failed = updateData.get("failed").toString()
                val queueId = updateData.get("queueId").toString()
                val type = updateData.get("type") as String

                if (type != "track") {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        updateBatchDownloadSubtext(queueId, size, downloaded, failed)
                        if (downloaded.toInt() + failed.toInt() == size.toInt()) {
                            downloadEndNotification(queueId, "Download Complete")
                        }
                    } else {
                        if (downloaded.toInt() + failed.toInt() == size.toInt()) {
                            updateBatchDownloadSubtext(queueId, size, downloaded, failed)
                            downloadEndNotification(queueId, "Download Complete")
                        }
                    }
                } else if (downloaded == size) {
                    downloadEndNotification(queueId, "Download Complete")
                } else if (failed == "1") {
                    downloadEndNotification(queueId, "Download Failed")
                }
            }

            // This is used for overall download progress on all download types
            on("downloadProgress") { args ->
                val updateData = args[0] as JSONObject

                val queueId = updateData.get("queueId").toString()
                var percentageDouble = updateData.get("percentage").toString().toDouble()

                if (percentageDouble < 0) {
                    percentageDouble = 0.toDouble()
                }

                val percentage = percentageDouble.toInt()

                if (percentage != 100) {
                    updateDownloadProgress(queueId, percentage)
                }
            }

            on("downloadComplete") { args ->
                scanNewSongInternal(args[0] as String)
            }
        }

        connectServiceSocket()
    }

    private fun connectServiceSocket() {
        try {
            NodeNative.serviceSocket.open()
        } catch (e: Exception) {
            Log.e(tag, "Socket failed to connect, err: ${e.printStackTrace()}")
        }
    }

    private var notifId = 100
    private var notifs: ArrayMap<String, ArrayMap<Int, NotificationCompat.Builder>> = ArrayMap()

    private fun newDownloadNotification(type: String, queueId: String, name: String, artist: String, size: String, downloaded: String = "0", failed: String = "0") {
        val thisBuilder = NotificationCompat.Builder(baseContext, downloadChannelID).apply {
            setSmallIcon(R.mipmap.ic_notification)
            setContentTitle("$name by $artist")
            setProgress(0, 0, true)
            setOnlyAlertOnce(true)
            setTimeoutAfter(6000000)
            setOngoing(true)
        }

        if (type != "track" && Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            thisBuilder.apply {
                setSubText("$downloaded/$size downloaded, $failed failed")
            }
        }

        val thisBuilderMap: ArrayMap<Int, NotificationCompat.Builder> = ArrayMap()

        thisBuilderMap[notifId] = thisBuilder

        notifs[queueId] = thisBuilderMap
        notifId++
        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )
    }

    private fun updateBatchDownloadSubtext(queueId: String, size: String = "1", downloaded: String = "0", failed: String = "0") {
        val thisBuilder = notifs.getValue(queueId).getValue(notifs.getValue(queueId).keyAt(0))

        thisBuilder.apply {
            setSubText("$downloaded/$size downloaded, $failed failed")
        }

        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )
    }

    private fun updateDownloadProgress(queueId: String, progress: Int) {
        val thisBuilder = notifs.getValue(queueId).getValue(notifs.getValue(queueId).keyAt(0))

        thisBuilder.apply {
            setProgress(100, progress, false)
        }

        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )
    }

    private fun downloadEndNotification(queueId: String, reason: String) {
        val thisBuilder = notifs.getValue(queueId).getValue(notifs.getValue(queueId).keyAt(0))

        thisBuilder.apply {
            setProgress(0, 0, false)
            setContentText(reason)
            setOngoing(false)
        }

        NotificationManagerCompat.from(baseContext).notify(
            queueId,
            notifs.getValue(queueId).keyAt(0),
            thisBuilder.build()
        )

        notifs.remove(queueId)
    }

    // Function to add new songs to the mediastore once fully downloaded
    private fun scanNewSongInternal(filePath: String) {
        MediaScannerConnection.scanFile(this,
                arrayOf(filePath),
                null
        ) { _, _ -> }
    }
}
