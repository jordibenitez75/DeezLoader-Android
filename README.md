# Deezloader For Android

Node.js Mobile version currently used: 0.2.1 (update this whenever updating Node.js Mobile)

Deezloader Remix asset version: [81d070833397bd79c9719cd65903b5c01da498d5](https://notabug.org/RemixDevs/DeezloaderRemix/commit/81d070833397bd79c9719cd65903b5c01da498d5)

Thanks to the power of the recent [Node.js for Mobile Apps](https://github.com/janeasystems/nodejs-mobile),
I'm glad to present to you the adapted version of Deezloader for Android!
Now you can forget about the Termux method to run this tool; just install the apk and you're ready to go!

**Telegram update notifications** - https://t.me/DeezLoaderAndroid

**Telegram community (bug reports and interaction with other members)** - https://t.me/joinchat/Ed1JxEfoci8sv2dVwTUQ3A

# Features
- Same as the desktop version, but adapted for the small screens.

# Downloads (Latest: 2.4.4)
- Download the latest release from [here](https://gitlab.com/Nick80835/DeezLoader-Android/tree/master/Release)

- P.D: I don't keep links to previous versions since it's easier to have the latest version online for you)

# How to build (prerequisite)
 - Download the latest version of [nodeJS mobile](https://github.com/janeasystems/nodejs-mobile/releases)
 - Copy the files from the .zip to the following path: "PROJECT_ROOT/app/libnode"

##### Minimum Android version: Android 5.0 Lollipop

# Crashes
In case there's a crash in the app, a notification will be displayed.
Open it and send it to [this Telegram group](https://t.me/joinchat/Ed1JxEfoci-dp-BWGRdVLg) to help improve the app.

# Credits
## Original Developer
[ZzMTV](https://boerse.to/members/zzmtv.3378614/)
## Past Maintainers
[ExtendLord](https://github.com/ExtendLord)<br/>
[ParadoxalManiak](https://github.com/ParadoxalManiak)<br/>
[snwflake](https://github.com/snwflake)

**No longer maintained by ZzMTV, ExtendLord, ParadoxalManiak, or snwflake**

# Disclaimer
- I am not responsible for the usage of this program by other people.
- I do not recommend you doing this illegally or against Deezer's terms of service.
- This project is licensed under [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/).
